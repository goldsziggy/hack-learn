const averages = require('./../output/averages.json')
const data = require('./../output/data.json')
const analyticsByDemo = require('./../output/analytics-by-demo.json')
const prediction = require('./../output/prediction.json')
const fs = require('fs')

console.log(data)

let frOverall = {}
let frByNo = {}
let frByDo = {}

Object.keys(data).forEach(function(frId) {
  const fr = data[frId]
  // num reviews, avg rating, num conversion
  const fieldRep = {
    id: frId,
    firstName: fr.firstName,
    lastName: fr.lastName,
    image: fr.image,
    NO: fr.NO,
    DO: fr.DO,
    numberOfReviews: averages[frId].totals.totalNumber,
    totalConversions: averages[frId].totals.totalConversions,
    conversionAverage: averages[frId].totals.conversion_rate,
    averageRating: averages[frId].totals.average_rating,
    analytics: analyticsByDemo[frId]
  }
  frOverall[frId] = fieldRep
  frByNo[fr.NO] = [...(frByNo[fr.NO] || []), fieldRep]
  frByDo[fr.DO] = [...(frByDo[fr.DO] || []), fieldRep]
})

fs.writeFileSync('./web-output/frOverall.json', JSON.stringify(frOverall), 'utf8')
fs.writeFileSync('./web-output/frByNO.json', JSON.stringify(frByNo), 'utf8')
fs.writeFileSync('./web-output/frByDO.json', JSON.stringify(frByDo), 'utf8')

// fs.writeFileSync('./fire-base-output/frOverall.json', JSON.stringify({ FRs: frOverall }), 'utf8')
// fs.writeFileSync('./fire-base-output/frByNO.json', JSON.stringify({ NOs: frByNo }), 'utf8')
// fs.writeFileSync('./fire-base-output/frByDO.json', JSON.stringify({ DOs: frByDo }), 'utf8')
