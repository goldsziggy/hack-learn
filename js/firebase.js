const { put } = require("axios");
const firebaseConfig = require("./firebase-config.json");

const frByDO = require("./web-output/frByDO.json");
const frByNO = require("./web-output/frByNO.json");
const frOverall = require("./web-output/frOverall.json");

const uploadToFirebase = async () => {
  try {
    console.log("Initialzing Firebase");

    await put(
      `https://homefield-hack-2.firebaseio.com/DOs.json?auth=${firebaseConfig.secret}`,
      frByDO
    )
      // .then(d => console.log({ d }))
      .catch(e => console.log({ e }));

    console.log("Loaded DOs");

    await put(
      `https://homefield-hack-2.firebaseio.com/NOs.json?auth=${firebaseConfig.secret}`,
      frByNO
    )
      // .then(d => console.log({ d }))
      .catch(e => console.log({ e }));

    console.log("Loaded NOs");
    await put(
      `https://homefield-hack-2.firebaseio.com/FRs.json?auth=${firebaseConfig.secret}`,
      frOverall
    )
      // .then(d => console.log({ d }))
      .catch(e => console.log({ e }));
    console.log("Loaded FRs");
  } catch (error) {
    console.log(error);
  }
};

uploadToFirebase();
