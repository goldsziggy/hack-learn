import json
from textacy import preprocessing
import re

# text = '🤡 email@gmail.com some. "string-here" $ 666-666-6666      '

# print('remove_accents', remove_accents)


def preprocessText(text):
    lowerText = text.lower()
    replace_emails = preprocessing.replace_emails(
        lowerText, replace_with='_EMAIL_')
    replace_currency_symbols = preprocessing.replace.replace_currency_symbols(
        replace_emails, replace_with='_CUR_')
    replace_emojis = preprocessing.replace.replace_emojis(
        replace_currency_symbols, replace_with='_EMOJI_')
    replace_phone_numbers = preprocessing.replace.replace_phone_numbers(
        replace_emojis, replace_with='_PHONE_')

    remove_punctuation = preprocessing.remove_punctuation(
        replace_phone_numbers)

    normalizeHyphenated = preprocessing.normalize.normalize_hyphenated_words(
        remove_punctuation)
    normalize_quotation_marks = preprocessing.normalize.normalize_quotation_marks(
        normalizeHyphenated)
    normalize_whitespace = preprocessing.normalize.normalize_whitespace(
        normalize_quotation_marks)

    remove_accents = preprocessing.remove.remove_accents(
        normalize_whitespace, fast=False)

    return remove_accents


FRs = {}


def getComment(clientData):
    return {
        'comment': preprocessText(clientData['comment']),
        'id': clientData['id']
    }


with open('data2.json') as json_file:
    data = json.load(json_file)
    for p in data['FRs']:
        comments = [getComment(clientData)
                    for clientData in data['FRs'][p]['clients']]
        combinedComments = ''

        for comment in comments:
            combinedComments += comment['comment']

        FRs[p] = {
            'clients': comments,
            'id': data['FRs'][p]['id'],
            'combinedComments': combinedComments
        }

with open('./output/processed-comments.json', 'w') as outfile:
    json.dump(FRs, outfile)
