import re
import json 
import yake

kw_extractor = yake.KeywordExtractor()


FRs = {}
FRsWithBag = {}

with open('./output/combinedComments.json') as json_file:
    data = json.load(json_file)
    for p in data['FRs']:
        FRs[p] = data['FRs'][p]
        print()

numOfRuns = 0
for frId, fr in FRs.items():
  numOfRuns += 1
  bagOfWords = []
  combinedWords = ''
  FRsWithBag[frId] = {}
  for d in fr['clients']:
    words = re.sub('[^A-Za-z]', ' ',d['comment'].lower())
    combinedWords += ' ' + words
  if combinedWords:
    try: 
      keywords = kw_extractor.extract_keywords(combinedWords)
      bagOfWords.append(keywords)
    except:
      print("An exception occurred")
      print (combinedWords)
      print ("")
  FRsWithBag[frId]['bagOfWords'] = bagOfWords
  print('FR Done: %d ' % numOfRuns)

with open('./output/bag-of-word-data.json', 'w') as outfile:
    json.dump(FRsWithBag, outfile)


print('hello world')