import json
import textacy
from textacy import resources, ke
import os.path

frDocs = {}
spacy_lang = textacy.load_spacy_lang("en")

dm = resources.DepecheMood(
    lang="en", data_dir='./textacy/')

#  @TODO: uncomment on first run!!!!
# dm.download()

frAnalytics = {}


def makeSpacyDocs(clientData):
    return {
        'comment': spacy_lang(clientData['comment']),
        'id': clientData['id']
    }


def makeBag(term, scake):
    score = [termTuple for termTuple in scake if termTuple[0] == term][0][1]
    return {
        'term': term,
        'score': score
    }


def fillBagWithWords(term_variants, scake, frId):
    bagOfWords = []
    for term_set in term_variants:
        for term in term_set:
            # print('term', term)
            bagOfWords.append([makeBag(term, scake)for term in term_set][0])

    frAnalytics[frId]['bagOfWords'] = bagOfWords


count = 0
with open('./output/processed-comments.json') as json_file:
    data = json.load(json_file)

    for fr, frData in data.items():
        count += 1
        frAnalytics[fr] = {}
        combinedDoc = spacy_lang(frData['combinedComments'])
        scake = ke.scake(combinedDoc)
        scakeSet = set([term for (term, score) in scake])
        term_variants = ke.utils.aggregate_term_variants(scakeSet)

        # print('scake\n', scake)
        # print('\nscakeSet\n', scakeSet)
        # print('term_variants', term_variants)
        print('filling bag of words!')
        fillBagWithWords(term_variants, scake, fr)
        print('bag filled, getting emotions')
        ev = dm.get_emotional_valence(combinedDoc)
        # print('\nEV\n', ev)
        frAnalytics[fr]['sentiment'] = ev

        # frDocs[fr] = {
        #     'individual': [makeSpacyDocs(clientData) for clientData in frData['clients']],
        #     'commented': combinedDoc
        # }
        # print('rs.get_synonyms(combinedDoc)', rs.get_synonyms(combinedDoc))
        # doc = spacy_lang(frData['combinedComments'])
        # print('preview\n', doc._.preview)
        # break
        print('count: ', count)

with open('./output/bag-of-word-data.json', 'w') as outfile:
    json.dump(frAnalytics, outfile)
# print('frDocs', frDocs)
# for fr in frDocs.items():
#     print('fr', fr)
#     break
