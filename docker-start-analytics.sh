#!/bin/bash
docker build -f Dockerfile.analytics -t hack-learn . && docker run --rm -v $(pwd)/textacy:/app/textacy -v $(pwd)/output:/app/output hack-learn
