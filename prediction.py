import re
import json 
import yake

kw_extractor = yake.KeywordExtractor()


FRs = {}
FRsClean = {}
Averages = {}
# Millennials,18 – 34 years
# Generation X (Roughly 35 – 50 years old
# Baby Boomers (Roughly 50 to 70 years old


age_grouping = {}

with open('data2.json') as json_file:
    data = json.load(json_file)
    for p in data['FRs']:
        FRs[p] = data['FRs'][p]
        print()

numOfRuns = 0
for frId, fr in FRs.items():
  numOfRuns += 1
  FRsClean[frId] = {}
  groups = { 'millennials': {'totalAge': 0, 'totalNumber': 0, 'totalRating': 0, 'totalConversions': 0, 'gender': {'M': { 'totalNumber': 0, 'totalRating': 0, 'totalConversions': 0, 'totalWorth': 0}, 'F': { 'totalNumber': 0, 'totalRating': 0, 'totalConversions': 0, 'totalWorth': 0,}}, 'totalWorth': 0, 'client_ids': []},
              'genx': {'totalAge': 0, 'totalNumber': 0, 'totalRating': 0, 'totalConversions': 0, 'gender': {'M': { 'totalNumber': 0, 'totalRating': 0, 'totalConversions': 0, 'totalWorth': 0}, 'F': { 'totalNumber': 0, 'totalRating': 0, 'totalConversions': 0, 'totalWorth': 0}}, 'totalWorth': 0, 'client_ids': []},
              'baby_boom': {'totalAge': 0, 'totalNumber': 0, 'totalRating': 0, 'totalConversions': 0,  'totalWorth': 0, 'gender': {'M': { 'totalNumber': 0, 'totalRating': 0, 'totalConversions': 0, 'totalWorth': 0}, 'F': { 'totalNumber': 0, 'totalRating': 0, 'totalConversions': 0, 'totalWorth': 0}}, 'client_ids': []},
              'M': { 'totalNumber': 0, 'totalRating': 0, 'totalConversions': 0, 'totalAge': 0, 'totalWorth': 0, 'client_ids': []},
              'F': { 'totalNumber': 0, 'totalRating': 0, 'totalConversions': 0, 'totalAge': 0, 'totalWorth': 0, 'client_ids': []},
              'totals': { 'totalNumber': 0, 'totalRating': 0, 'totalConversions': 0, 'totalAge': 0, 'totalWorth': 0},
              }
  client_ids = []
  for d in fr['clients']:
    
    client_id = d['id']
    age = d['age']
    sex = d['sex']
    rating = d['rating']
    is_converted = d['isConverted']
    net_worth = float(d['networth']) if is_converted else 0
    conversion_counter = 1 if is_converted else 0
    client_ids.append(client_id)


    if age <= 34:
      # Millennials
      groups['millennials']['totalNumber'] += 1
      groups['millennials']['totalAge'] += age
      groups['millennials']['totalRating'] += rating
      groups['millennials']['totalConversions'] += conversion_counter     
      groups['millennials']['totalWorth'] += net_worth   
      groups['millennials']['client_ids'].append(client_id)      
    elif age <= 50:
      # GenX
      groups['genx']['totalNumber'] += 1
      groups['genx']['totalAge'] += age
      groups['genx']['totalRating'] += rating
      groups['genx']['totalConversions'] += conversion_counter      
      groups['genx']['totalWorth'] += net_worth
      groups['genx']['client_ids'].append(client_id)      
    else:
      # baby boomers
      groups['baby_boom']['totalNumber'] += 1
      groups['baby_boom']['totalAge'] += age
      groups['baby_boom']['totalRating'] += rating
      groups['baby_boom']['totalConversions'] += conversion_counter    
      groups['baby_boom']['totalWorth'] += net_worth  
      groups['baby_boom']['client_ids'].append(client_id)      
    groups[sex]['client_ids'].append(client_id)       
    groups[sex]['totalNumber'] += 1
    groups[sex]['totalAge'] += age
    groups[sex]['totalRating'] += rating
    groups[sex]['totalConversions'] += conversion_counter
    groups[sex]['totalWorth'] += net_worth

    groups['totals']['totalWorth'] += net_worth
    groups['totals']['totalNumber'] += 1
    groups['totals']['totalAge'] += age
    groups['totals']['totalRating'] += rating
    groups['totals']['totalConversions'] += conversion_counter
  groups['client_ids'] = client_ids
  FRsClean[frId] = groups
  print('FR Done: %d ' % numOfRuns)

def getAvg (data):
  total_num = data['totalNumber']
  total_age = data['totalAge']
  total_rating = data['totalRating']
  total_conversions = data['totalConversions']
  total_worth = data['totalWorth']
  if total_num > 0:
    average_age = total_age / total_num
    average_rating = total_rating / total_num
    average_worth = total_worth / total_conversions if total_conversions > 0 else 0
    conversion_rate = total_conversions / total_num
    return { 'average_age': average_age, 'average_rating': average_rating, 'conversion_rate': conversion_rate, 'average_worth': average_worth, **data }
  return { 'average_age': 0, 'average_rating': 0, 'conversion_rate': 0, 'average_worth': 0, **data }


numOfRuns = 0
for frId, fr in FRsClean.items():
  numOfRuns += 1 
  # print(fr)
  Averages[frId] = {}
  Averages[frId]['client_ids'] = fr['client_ids']
  Averages[frId]['millennials'] =  getAvg(fr["millennials"])
  Averages[frId]['genx'] =  getAvg(fr["genx"])
  Averages[frId]['baby_boom'] =  getAvg(fr["baby_boom"])
  Averages[frId]['M'] =  getAvg(fr["M"])
  Averages[frId]['F'] =  getAvg(fr["F"])
  Averages[frId]['totals'] =  getAvg(fr["totals"])
  print()


print(Averages)

print('Processing Done!')

with open('./output/prediction.json', 'w') as outfile:
    json.dump(Averages, outfile)


print('File Write Done!')