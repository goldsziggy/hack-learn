import json
import textacy
from textacy import resources, ke
import os.path

# frDocs = {}
spacy_lang = textacy.load_spacy_lang("en")

dm = resources.DepecheMood(
    lang="en", data_dir='./textacy/')

#  @TODO: uncomment on first run!!!!
# dm.download()

frAnalytics = {}


def makeSpacyDocs(clientData):
    return {
        'comment': spacy_lang(clientData['comment']),
        'id': clientData['id']
    }


def makeBag(term, scake):
    score = [termTuple for termTuple in scake if termTuple[0] == term][0][1]
    return {
        'term': term,
        'score': score
    }


def fillBagWithWords(term_variants, scake, frId, clientType, doc):
    bagOfWords = []
    for term_set in term_variants:
        for term in term_set:
            # print('term', term)
            bagOfWords.append([makeBag(term, scake)for term in term_set][0])

    frAnalytics[frId][clientType] = {
        'bag-of-words': bagOfWords,
        'sentiment': dm.get_emotional_valence(doc)
    }


prediction = None
with open('./output/prediction.json') as prediction_json:
    prediction = json.load(prediction_json)

with open('./output/processed-comments.json') as json_file:
    data = json.load(json_file)
    millennials = 'millennials'
    genx = 'genx'
    baby_boom = 'baby_boom'

    for fr, frData in data.items():
        currentFr = prediction[fr]
        currentMillenialIds = currentFr[millennials]['client_ids']
        currentGenxIds = currentFr[genx]['client_ids']
        currentBabyBoomIds = currentFr[baby_boom]['client_ids']

        frAnalytics[fr] = {}

        currentMillenialComments = [clientData['comment']
                                    for clientData in frData['clients'] if clientData['id'] in currentMillenialIds]
        currentGenxComments = [clientData['comment']
                               for clientData in frData['clients'] if clientData['id'] in currentGenxIds]
        currentBabyBoomComments = [clientData['comment']
                                   for clientData in frData['clients'] if clientData['id'] in currentBabyBoomIds]

        millennialsCombined = ''
        for m in currentMillenialComments:
            millennialsCombined += ' ' + m

        genxCombined = ''
        for g in currentGenxComments:
            genxCombined += ' ' + g

        babyBoomCombined = ''
        for b in currentBabyBoomComments:
            babyBoomCombined += ' ' + b

        allcombined = babyBoomCombined + ' ' + genxCombined + ' ' + millennialsCombined

        millennialsDoc = spacy_lang(millennialsCombined)
        genxDoc = frDatespacy_lang(genxCombined)
        baby_boomDoc = spacy_lang(babyBoomCombined)

        all_doc = spacy_lang(allcombined)

        # combinedDoc = spacy_lang(frData['combinedComments'])

        millennialsScake = ke.scake(millennialsDoc)
        millennialsScakeSet = set([term for (term, score) in millennialsScake])
        millennialsTerm_variants = ke.utils.aggregate_term_variants(
            millennialsScakeSet)

        genxScake = ke.scake(genxDoc)
        genxScakeSet = set([term for (term, score) in genxScake])
        genxsTerm_variants = ke.utils.aggregate_term_variants(
            genxScakeSet)

        babyBoomScake = ke.scake(baby_boomDoc)
        babyBoomScakeSet = set([term for (term, score) in babyBoomScake])
        babyBoomTerm_variants = ke.utils.aggregate_term_variants(
            babyBoomScakeSet)

        allScake = ke.scake(all_doc)
        allScakeSet = set([term for (term, score) in allScake])
        allTerm_variants = ke.utils.aggregate_term_variants(
            allScakeSet)
        # print('scake\n', scake)
        # print('\nscakeSet\n', scakeSet)
        # print('term_variants', term_variants)

        fillBagWithWords(millennialsTerm_variants,
                         millennialsScake, fr, millennials, millennialsDoc)
        fillBagWithWords(genxsTerm_variants, genxScake, fr, genx, genxDoc)        
        fillBagWithWords(babyBoomTerm_variants, babyBoomScake,
                         fr, baby_boom, baby_boomDoc)
        fillBagWithWords(allTerm_variants, allScake,
                         fr, 'all', all_doc)

        # mev = dm.get_emotional_valence(millennialsDoc)
        # gev = dm.get_emotional_valence(genxDoc)
        # bev = dm.get_emotional_valence(baby_boomDoc)
        # # print('\nEV\n', ev)
        # frAnalytics[fr]['sentiment'] = {}
        # frAnalytics[fr]['sentiment'][millennials] = mev
        # frAnalytics[fr]['sentiment'][genx] = gev
        # frAnalytics[fr]['sentiment'][baby_boom] = bev

        # print('frAnalytics', frAnalytics)
        # frDocs[fr] = {
        #     'individual': [makeSpacyDocs(clientData) for clientData in frData['clients']],
        #     'commented': combinedDoc
        # }
        # break

with open('./output/analytics-by-demo.json', 'w') as outfile:
    json.dump(frAnalytics, outfile)
