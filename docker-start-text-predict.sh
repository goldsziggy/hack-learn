#!/bin/bash
docker build -f Dockerfile.textacy-predict -t hack-learn . && docker run --rm -v $(pwd)/textacy:/app/textacy -v $(pwd)/output:/app/output hack-learn
